package ch.hesso.remembeer.database.entity;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Entite Brasserie
 */

public class BreweryEntity {
    private String idBrewery;
    private String name;
    private String address;
    private String city;
    private String image;
    private String web;

    private boolean favoris = false;
    private String description;



    @Exclude
    public String getIdBrewery() {
        return idBrewery;
    }

    public void setIdBrewery(String idBrewery) {
        this.idBrewery = idBrewery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getImage() {return image; }

    public void setImage(String image) { this.image = image; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavoris() {
        return favoris;
    }

    public void setFavoris(boolean favoris) {
        this.favoris = favoris;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }


    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("address", address);
        result.put("image", image);
        result.put("city", city);
        result.put("web", web);
        result.put("description", description);
        result.put("favoris", favoris);
        //result.put("Beers", description);
        return result;
    }

}
